package com.agileengine.exceptions;

public class ImageProcessException extends RuntimeException {
    public ImageProcessException() {
        super();
    }

    public ImageProcessException(String message) {
        super(message);
    }
}
