package com.agileengine.exceptions;

public class PropertiesException extends RuntimeException {
    public PropertiesException() {
        super();
    }

    public PropertiesException(String message) {
        super(message);
    }
}
