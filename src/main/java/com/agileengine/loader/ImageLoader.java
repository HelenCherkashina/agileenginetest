package com.agileengine.loader;

import com.agileengine.exceptions.ImageProcessException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class ImageLoader {
    public static List<BufferedImage> loadImages(List<File> files) {
        return files.stream().map(it -> {
            try(InputStream inputStream = new FileInputStream(it.getAbsolutePath())) {
                return ImageIO.read(inputStream);
            } catch (IOException e) {
                throw new ImageProcessException(String.format("Couldn't get file by path %s", it.getAbsolutePath()));
            }
        }).collect(Collectors.toList());
    }

    public static File saveImage(BufferedImage image) {
        File result = new File("resultImage.png");
        try(OutputStream outputStream = new FileOutputStream(result)) {
            ImageIO.write(image, "png", outputStream);
            return result;
        } catch (IOException e) {
            throw new ImageProcessException("Couldn't save result file");
        }
    }
}
