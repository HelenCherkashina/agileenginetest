package com.agileengine;

import com.agileengine.frame.ImageFrame;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(() -> {
            JFrame.setDefaultLookAndFeelDecorated(true);
            JDialog.setDefaultLookAndFeelDecorated(true);
            new ImageFrame().createPanel();
        });
    }
}
