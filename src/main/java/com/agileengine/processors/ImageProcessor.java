package com.agileengine.processors;

import com.agileengine.exceptions.ImageProcessException;
import com.agileengine.models.Pixel;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

public class ImageProcessor {
    private static final Double MIN_DIFFERENCE_IN_PIXELS = 0.1;

    public BufferedImage highlightDifferencesOnImage(List<BufferedImage> images) {
        if(images == null || images.size() == 0)
        {
            throw new ImageProcessException("No files were selected");
        }

        BufferedImage firstImage = images.get(0);
        List<Pixel> differentPixels = findDifferentPixels(firstImage, images.get(1));
        differentPixels.sort(Comparator.comparing(Pixel::getX).thenComparing(Pixel::getY));
        ListIterator<Pixel> iterator = differentPixels.listIterator();

        Graphics2D imageWithDifference = firstImage.createGraphics();
        imageWithDifference.setColor(Color.RED);

        Pixel start = null;
        while (iterator.hasNext()) {
            if (!iterator.hasPrevious()) {
                start = differentPixels.get(iterator.nextIndex());
                iterator.next();
                continue;
            }

            Pixel current = differentPixels.get(iterator.nextIndex());
            Pixel previous = differentPixels.get(iterator.previousIndex());
            if (start != null && (current.getX() - previous.getX()) > 1) {
                int width = previous.getX() - start.getX();
                int height = previous.getY() - start.getY();
                imageWithDifference.drawRect(start.getX(), start.getY(), width, height);
                start = current;
            }
            iterator.next();
        }

        imageWithDifference.dispose();
        return firstImage;
    }

    private List<Pixel> findDifferentPixels(BufferedImage firstImage, BufferedImage secondImage) {
        if (firstImage == null || secondImage == null) {
            throw new ImageProcessException("Couldn't process given images");
        }

        List<Pixel> differentPixels = new ArrayList<>();
        for (int x = 0; x < firstImage.getWidth(); x++) {
            for (int y = 0; y < firstImage.getHeight(); y++) {
                int firstImagePixel = Math.abs(firstImage.getRGB(x, y));
                int secondImagePixel = Math.abs(secondImage.getRGB(x, y));

                int maxRgb = Math.max(firstImagePixel, secondImagePixel);
                int minRgb = Math.min(firstImagePixel, secondImagePixel);

                if ((1 - (minRgb / maxRgb)) >= MIN_DIFFERENCE_IN_PIXELS) {
                    differentPixels.add(new Pixel(x, y));
                }
            }
        }

        return differentPixels;
    }
}
