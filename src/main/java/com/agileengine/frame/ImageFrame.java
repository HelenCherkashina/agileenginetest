package com.agileengine.frame;

import com.agileengine.exceptions.ImageProcessException;
import com.agileengine.loader.ImageLoader;
import com.agileengine.processors.ImageProcessor;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class ImageFrame extends JFrame {
    private static PropertiesLoader propertiesLoader = PropertiesLoader.getInstance();
    private File[] files;

    public ImageFrame() {
        super(propertiesLoader.getProperty("frame.name"));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public JPanel createPanel() {
        JLabel label = new JLabel(propertiesLoader.getProperty("frame.selected.files.label"));
        label.setAlignmentX(CENTER_ALIGNMENT);

        JPanel panel = setUpPanel();
        panel.add(label);
        panel.add(createSelectButton(label));
        panel.add(createShowDifferenceButton());
        panel.add(Box.createVerticalGlue());
        getContentPane().add(panel);

        setPreferredSize(new Dimension(260, 220));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);

        return panel;
    }

    private JPanel setUpPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(Box.createVerticalGlue());
        panel.add(Box.createRigidArea(new Dimension(10, 10)));
        return panel;
    }

    private JButton createSelectButton(JLabel label) {
        JButton selectButton = new JButton(propertiesLoader.getProperty("select.button.name"));
        selectButton.setAlignmentX(CENTER_ALIGNMENT);
        selectButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setMultiSelectionEnabled(true);
            int ret = fileChooser.showDialog(null, propertiesLoader.getProperty("select.dialog.window.name"));
            if (ret == JFileChooser.APPROVE_OPTION) {
                File[] selectedFiles = fileChooser.getSelectedFiles();
                if (selectedFiles == null || selectedFiles.length != 2) {
                    JOptionPane.showMessageDialog(null, propertiesLoader.getProperty("message.selected.not.two.files"));
                    return;
                }
                files = selectedFiles;
                List<String> fileNames = Arrays.stream(files).map(File::getName).collect(Collectors.toList());
                label.setText(String.join("; ", fileNames));
            }
        });
        return selectButton;
    }

    private JButton createShowDifferenceButton() {
        JButton showDifferencesButton = new JButton(propertiesLoader.getProperty("show.difference.button.name"));
        showDifferencesButton.setAlignmentX(CENTER_ALIGNMENT);
        showDifferencesButton.addActionListener(e -> {
            if (files == null) {
                JOptionPane.showMessageDialog(null, propertiesLoader.getProperty("message.no.files.selected"));
                return;
            }
            try {
                List<BufferedImage> bufferedImages = ImageLoader.loadImages(Arrays.asList(files));
                BufferedImage result = new ImageProcessor().highlightDifferencesOnImage(bufferedImages);
                File resultFile = ImageLoader.saveImage(result);
                JOptionPane.showMessageDialog(null, String.format(
                        propertiesLoader.getProperty("message.file.successfully.saved"), resultFile.getAbsolutePath()));
            } catch (ImageProcessException ex) {
                JOptionPane.showMessageDialog(null,
                        propertiesLoader.getProperty("message.error.while.processing.images"));
            }
        });

        return showDifferencesButton;
    }
}
