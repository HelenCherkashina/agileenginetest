package com.agileengine.frame;

import com.agileengine.exceptions.PropertiesException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {
    private static final String PROPERTIES_FILE_NAME = "/application.properties";

    private static PropertiesLoader propertiesLoader = null;
    private Properties properties = null;

    private PropertiesLoader() {
        properties = new Properties();
        try {
            InputStream inputStream = getClass().getResourceAsStream(PROPERTIES_FILE_NAME);
            properties.load(inputStream);
        } catch (IOException e) {
            throw new PropertiesException(String.format("Couldn't load properties from file %s", PROPERTIES_FILE_NAME));
        }
    }

    public static synchronized PropertiesLoader getInstance() {
        return propertiesLoader == null ? new PropertiesLoader() : propertiesLoader;
    }

    public synchronized String getProperty(String key) {
        return properties.containsKey(key) ? (String) properties.get(key) : null;
    }
}
